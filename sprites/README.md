# Sprite images
These images were used to generate the sprites for TinyTunes. The sprites are encoded in the Python code, so they're only here in case I decide to redesign any of them.
