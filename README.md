# TinyTunes
A music editor for the Thumby console.

If you can't read music, don't worry, just experiment. You can write a tune!

## Screen shots
Here's TinyTunes in the Thumby emulator, and a close up of the screen:

![Thumby emulator showing a D major arpeggio](https://codeberg.org/JBanana/tinytunes/raw/branch/main/screenshot1.png "Thumby emulator showing a D major arpeggio") ![D major arpeggio](https://codeberg.org/JBanana/tinytunes/raw/branch/main/screenshot2.png "D major arpeggio")

## Status
* Working: you can edit, play, save and load tunes.

## To run this
First, you need a Thumby. It's an extremely small game console.

https://thumby.us

1. Clone this repo
1. Open the Thumby code editor - https://code.thumby.us
1. Make directory `/Games/TinyTunes` on the Thumby (note casing)
1. Import `TinyTunes.py` from your cloned repo
1. Save it to `/Games/TinyTunes`

Now you should be able to create tunes on your Thumby.

You could also edit music in the emulator, but you can't save a tune so there's not much point. I don't hear any sound when the tunes plays in the emulator. I'm puzzled, because the emulator has a Mute button, so it looks like sound should work. [shrug]

## Issues
Audio on my Thumby doesn't always work. Usually the notes play; sometimes not. I don't know if I have a dodgy Thumby or whether they're all like that. If you don't hear anything, try again a few times.
